(ns cljtest.core
  (:use clojure.set))

(defn neighbor-indexes [i]
  "All indexes of the neighbors excluding i"
  (let [r (int (/ i 9))
        c (mod i 9)
        gr (int (/ r 3))
        gc (int (/ c 3))
        row-indexes (map #(+ (* r 9) %) (range 9))
        col-indexes (map #(+ (* % 9) c) (range 9))
        block-indexes
        (for [ri (range (* (int gr) 3) (* (+ (int gr) 1) 3))
              ci (range (* (int gc) 3) (* (+ (int gc) 1) 3))]
          (+ (* ri 9) ci))]
    (disj (into #{} (flatten [row-indexes col-indexes block-indexes])) i)))
        
(defn constraints [si i]
  (let [s (partition 9 si) 
        r (/ i 9) 
        c (mod i 9) 
        gc (/ c 3) 
        gr (/ r 3)
        every-nth (fn [s2 i] (take-nth 9 (drop i s2)))
        every-nth3 (fn [s2 i] (take-nth 3 (drop i s2)))
        grp-col (every-nth3 (map #(partition 3 %) s) gc)
        grp (take 3 (drop (* 3 (int gr)) grp-col))]
    (into #{} (flatten [(nth s r) (every-nth si c) grp]))))

(defn index-of-least-candidates [s i index-of-least count-of-least]
  (if (< i 81)
    (if (= 0 (nth s i))
      (let [new-count (- 9 (count (disj (constraints s i) 0)))]
        (case new-count
          0 i ; This means that there is conflict, must backtrack
          1 i ; one candidate only, select this
          (if (< new-count count-of-least)
            (index-of-least-candidates s (inc i) i new-count)
            (index-of-least-candidates s (inc i) index-of-least count-of-least))))
      (index-of-least-candidates s (inc i) index-of-least count-of-least))
    index-of-least))

(defn solve-brute1 [s]
  (if (.contains s 0)
    (let [i (.indexOf s 0)
          inject #(concat (take %2 %1) [%3] (drop (inc %2) %1))]
      (flatten (map #(solve-brute1 (inject s i %))
                    (difference #{1 2 3 4 5 6 7 8 9} (constraints s i))))) 
    s))

(defn solve-brute2 [s]
  (if (.contains s 0)
    (let [i (index-of-least-candidates s (.indexOf s 0) 0 10)
          inject #(concat (take %2 %1) [%3] (drop (inc %2) %1))]
      (flatten (map #(solve-brute2 (inject s i %))
                    (difference #{1 2 3 4 5 6 7 8 9} (constraints s i))))) 
    s))

(def easy 
  [3 0 0 0 0 5 0 1 0
   0 7 0 0 0 6 0 3 0
   1 0 0 0 9 0 0 0 0
   7 0 8 0 0 0 0 9 0
   9 0 0 4 0 8 0 0 2
   0 6 0 0 0 0 5 0 1
   0 0 0 0 4 0 0 0 6
   0 4 0 7 0 0 0 2 0
   0 2 0 6 0 0 0 0 3])

(def hardestsudokuinworld 
  [8 5 0 0 0 2 4 0 0 
   7 2 0 0 0 0 0 0 9 
   0 0 4 0 0 0 0 0 0 
   0 0 0 1 0 7 0 0 2 
   3 0 5 0 0 0 9 0 0 
   0 4 0 0 0 0 0 0 0 
   0 0 0 0 8 0 0 7 0 
   0 1 7 0 0 0 0 0 0 
   0 0 0 0 3 6 0 4 0])

(def sudokuwiki-unsolvable-28 
  [6 0 0 0 0 8 9 4 0 
   9 0 0 0 0 6 1 0 0 
   0 7 0 0 4 0 0 0 0 
   2 0 0 6 1 0 0 0 0 
   0 0 0 0 0 0 2 0 0 
   0 8 9 0 0 2 0 0 0 
   0 0 0 0 6 0 0 0 5 
   0 0 0 0 0 0 0 3 0 
   8 0 0 0 0 1 6 0 0])
(def sudokuwiki-unsolvable-28-solution 
  [6 2 5 1 7 8 9 4 3 
   9 4 8 3 2 6 1 5 7 
   3 7 1 9 4 5 8 6 2 
   2 5 7 6 1 9 3 8 4 
   4 6 3 5 8 7 2 9 1 
   1 8 9 4 3 2 5 7 6 
   7 9 2 8 6 3 4 1 5 
   5 1 6 2 9 4 7 3 8 
   8 3 4 7 5 1 6 2 9])

; (solve hardestsudokuinworld)
