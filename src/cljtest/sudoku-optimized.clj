(ns cljtest.core (:require [taoensso.timbre :as timbre]))
(timbre/refer-timbre)

(defn calculate-neighbor-indexes [i]
  "Return set of indexes of the neighbors excluding i itself. 0 <= i < 81"
  (let [r (int (/ i 9))
        c (mod i 9)
        gr (int (/ r 3))
        gc (int (/ c 3))
        row-indexes (map #(+ (* r 9) %) (range 9))
        col-indexes (map #(+ (* % 9) c) (range 9))
        block-indexes
        (for [ri (range (* (int gr) 3) (* (+ (int gr) 1) 3))
              ci (range (* (int gc) 3) (* (+ (int gc) 1) 3))]
          (+ (* ri 9) ci))]
    (disj (into #{} (flatten [row-indexes col-indexes block-indexes])) i)))

(def neighbors-table
  (vec (for [i (range 81)] (calculate-neighbor-indexes i))))

(defn candidates [board neighbor-indexes]
  "Return set of free numbers (candidates) for this cell."
  (reduce (fn [s i] 
            (let [cellval (nth board i)] 
              (if (> cellval 0) 
                (disj s cellval) 
                s)))
          #{1 2 3 4 5 6 7 8 9} neighbor-indexes))

(defn find-least-candidates [board neighbors-table]
  "Return [least-i least-count] or nil if conflict found"
  (reduce (fn [[old-i old-count] i]
            (if (= 0 (nth board i))
              (let [new-count (p :cand-count (count (candidates board (nth neighbors-table i))))]
                (if (<= new-count 1)
                  (reduced [i new-count])
                  (if (< new-count old-count)
                    [i new-count]
                    [old-i old-count])))
              [old-i old-count]))
          [81 10]
          (range 81)))

; Tämä on n. 2-kertaa nopeampi kuin sudoku.clj:solve
(defn solve-optimized [s]
  (if (.contains s 0)
    (let [[i count] (p :find-least (find-least-candidates s neighbors-table))]
      (some #(solve-optimized (assoc s i %))
            (p :candidates (candidates s (nth neighbors-table i))))) 
    s))

(def easy
  [3 0 0 0 0 5 0 1 0
   0 7 0 0 0 6 0 3 0
   1 0 0 0 9 0 0 0 0
   7 0 8 0 0 0 0 9 0
   9 0 0 4 0 8 0 0 2
   0 6 0 0 0 0 5 0 1
   0 0 0 0 4 0 0 0 6
   0 4 0 7 0 0 0 2 0
   0 2 0 6 0 0 0 0 3])

(def hardestsudokuinworld 
  [8 5 0 0 0 2 4 0 0 
   7 2 0 0 0 0 0 0 9 
   0 0 4 0 0 0 0 0 0 
   0 0 0 1 0 7 0 0 2 
   3 0 5 0 0 0 9 0 0 
   0 4 0 0 0 0 0 0 0 
   0 0 0 0 8 0 0 7 0 
   0 1 7 0 0 0 0 0 0 
   0 0 0 0 3 6 0 4 0])

(def sudokuwiki-unsolvable-28 
  [6 0 0 0 0 8 9 4 0 
   9 0 0 0 0 6 1 0 0 
   0 7 0 0 4 0 0 0 0 
   2 0 0 6 1 0 0 0 0 
   0 0 0 0 0 0 2 0 0 
   0 8 9 0 0 2 0 0 0 
   0 0 0 0 6 0 0 0 5 
   0 0 0 0 0 0 0 3 0 
   8 0 0 0 0 1 6 0 0])
(def sudokuwiki-unsolvable-28-solution 
  [6 2 5 1 7 8 9 4 3 
   9 4 8 3 2 6 1 5 7 
   3 7 1 9 4 5 8 6 2 
   2 5 7 6 1 9 3 8 4 
   4 6 3 5 8 7 2 9 1 
   1 8 9 4 3 2 5 7 6 
   7 9 2 8 6 3 4 1 5 
   5 1 6 2 9 4 7 3 8 
   8 3 4 7 5 1 6 2 9])

(time (solve-optimized hardestsudokuinworld))
;(profile :info :Arithmetic (solve-optimized hardestsudokuinworld))
