(ns cljtest.core)

; Sudoku board is vector of vectors [row1, row2, ...]
; cell with value is just integer, 
; cell without value is set of possible values #{1 2 3 4 5 6 7 8 9}
;
; r = 0..8
; c = 0..8
; value = 1..9

(defn new-board []
  (vec (for [row (range 9)]
         (vec (for [col (range 9)] 
                (set (range 1 10)))))))

(defn get-cell [board r c]
  (nth (nth board r) c))
(defn get-row [board r]
  (nth board r))
(defn get-col [board c]
  (map #(nth % c) board))
(defn get-block [board r c]
  (for [ri (range (* (int (/ r 3)) 3) (* (+ (int (/ r 3)) 1) 3))
        ci (range (* (int (/ c 3)) 3) (* (+ (int (/ c 3)) 1) 3))]
    (get-cell board ri ci)))
(defn in-same-block? [r1 c1 r2 c2]
  (and (= (int (/ r1 3)) (int (/ r2 3)))
       (= (int (/ c1 3)) (int (/ c2 3)))))

(defn pretty-print-cell [cell]
  (format "%10s" (if (set? cell) (apply str cell) (str "val: " cell))))
(defn pretty-print-row [row]
  (apply pr-str (map pretty-print-cell row) "\n"))
(defn pretty-print-board [board]
  (map pretty-print-row board))

(defn value-conflicts-with-cell? [cell value]
  (if (set? cell) nil (= cell value)))
(defn value-conflicts-with-block? [block value]
  (some #(value-conflicts-with-cell? % value) block))
(defn value-conflicts-with-board? [board r c val]
  (let [row (get-row board r)
        col (get-col board c)
        block (get-block board r c)]
    (or (value-conflicts-with-block? row val)
        (value-conflicts-with-block? col val)
        (value-conflicts-with-block? block val))))

; Remove val from free values in all cells this row, col and block
; Return new board
(defn set-value [board r c val]
  (if (> val 0)
    (vec (for [row (range 9)]
           (vec (for [col (range 9)] 
                  (cond 
                    (and (= r row) (= c col)) val
                    (not (set? (get-cell board row col))) (get-cell board row col)
                    (or (= r row) (= c col) (in-same-block? row col r c)) (disj (get-cell board row col) val)
                    :else (get-cell board row col))))))
    board))

; Return number of candidates or 10 if cell is already set
(defn get-num-candidates [cell]
  (if (set? cell) (count cell) 10))

; returns (index cell)
(defn min-with-index [row get-value-fn]
  (apply min-key #(get-value-fn (second %)) (map-indexed vector row)))

; returns r,c index to cell with least candidates and cell itself (r c cell)
(defn find-cell-with-least-candidates [board]
  (flatten (apply min-key #(get-num-candidates (second (second %))) 
                  (map-indexed vector (map #(min-with-index % get-num-candidates) board)))))

(defn num-free-cells [board]
  (reduce + (map #(count (filter set? %)) board)))

; point of highest abstraction
; Recursive depth-first search of solution
; returns solved board with first found solution
(defn solve [board]
  (if (> (num-free-cells board) 0)
    (let [[r c cell] (find-cell-with-least-candidates board)]
      (some #(if (value-conflicts-with-board? board r c %)
                 nil 
                 (solve (set-value board r c %)))
              cell)
        )
    board))

(defn read-row [r chars board]
  (reduce (fn [brd [c val]] (set-value brd r c val)) 
          board (map-indexed vector (map #(read-string (str %)) chars))))
(defn read-sudoku [sudoku-string]
  (reduce (fn [board [r chars]] (read-row r chars board)) (new-board) (map-indexed vector (partition 9 sudoku-string))))

;; TESTS
(def hardestsudokuinworld "850002400720000009004000000000107002305000900040000000000080070017000000000036040")
(def sudokuwiki-unsolvable-28 "600008940900006100070040000200610000000000200089002000000060005000000030800001600")
(def sudokuwiki-unsolvable-28-solution "625178943948326157371945862257619384463587291189432576792863415516294738834751629")

(let [s1 (read-sudoku hardestsudokuinworld)]
  (println "Hardest Sudoku in the world:")
  (println (pretty-print-board s1))
  (println "solution:")
  (println (time (pretty-print-board (solve s1)))))

(let [sudoku (read-sudoku sudokuwiki-unsolvable-28)]
  (println "Unsolvable #28, sudokuwiki.org:")
  (println (pretty-print-board sudoku))
  (println "solution:")
  (let [reference (read-sudoku sudokuwiki-unsolvable-28-solution)
        solution (time (solve sudoku))]
    (println (pretty-print-board solution))
    (println "Equals with reference solution: " (= reference solution))))








