(ns cljtest.core)

; Todennäköisyys kahden nopan tuloksille
;  - Kahden nopan summa on vähintään 9
;    (two-dice-probability + #(>= % 9)) => 5/18
;  - Vähintään toisen nopan silmäluku 5
;    (two-dice-probability #(set (list %1 %2)) #(contains? % 5)) => 11/36
(defn two-dice-probability [combine pred]
  (let [all-elems (for [d1 (range 1 7), d2 (range 1 7)] (combine d1 d2))]
    (/ (count (filter pred all-elems))
       (count all-elems))))

  