(ns cljtest.core (:require [taoensso.timbre :as timbre]))
(timbre/refer-timbre)

(defn calculate-neighbor-indexes [i]
  "Return set of indexes of the neighbors excluding i itself. 0 <= i < 81"
  (let [r (int (/ i 9))
        c (mod i 9)
        gr (int (/ r 3))
        gc (int (/ c 3))
        row-indexes (map #(+ (* r 9) %) (range 9))
        col-indexes (map #(+ (* % 9) c) (range 9))
        block-indexes
        (for [ri (range (* (int gr) 3) (* (+ (int gr) 1) 3))
              ci (range (* (int gc) 3) (* (+ (int gc) 1) 3))]
          (+ (* ri 9) ci))]
    (disj (into #{} (flatten [row-indexes col-indexes block-indexes])) i)))

(def neighbors-table
  (vec (for [i (range 81)] (calculate-neighbor-indexes i))))

; Sudoku board is 1-dimensional vector 
; cell with value is just integer, 
; cell without value is set of possible values #{1 2 3 4 5 6 7 8 9}
;
; r = 0..8
; c = 0..8
; value = 1..9

(defn new-board []
  (vec (for [i (range 81)]
         (set (range 1 10)))))

(defn pretty-print-cell [cell]
  (format "%10s" (if (set? cell) (apply str cell) (str "val: " cell))))
(defn pretty-print-row [row]
  (apply pr-str (map pretty-print-cell row) "\n"))
(defn pretty-print-board [board]
  (map pretty-print-row (partition 9 board)))

(defn set-value [board n new-value]
  "Return new board with cell n = new-value and new-value removed from all neighbors"
  (p :set-value 
     (let [neighbor-indexes (nth neighbors-table n)
           tboard (transient board)]
       (assoc! tboard n new-value)
       (doseq [i neighbor-indexes]
         (let [cell (nth board i)]
           (if (set? cell) 
             (assoc! tboard i (disj cell new-value)))))  
       (persistent! tboard))))

(defn get-num-candidates [board i]
  "Return number of candidates or 10 if cell is already set 
   or 0 if cell is not set but there are no candidates left (conflict)"
  (let [cell (nth board i)]
    (if (set? cell) 
      (count cell) 
      10)))

(defn find-least-candidates [board]
  "Return [least-i least-count] or [i 0] if conflict found or [81 10] if board is solved"
  (reduce (fn [[old-i old-count] i]
            (if (set? (nth board i))
              (let [new-count (get-num-candidates board i)]
                (cond
                  (= new-count 0) [i 0]
                  (= new-count 1) (reduced [i new-count])
                  :else (if (< new-count old-count)
                          [i new-count]
                          [old-i old-count])))
              [old-i old-count]))
          [81 10]
          (range 81)))

(defn read-sudoku [sudoku-string]
  (reduce (fn [brd [i val]] (if (> val 0) (set-value brd i val) brd)) 
          (new-board) (map-indexed vector (map #(read-string (str %)) sudoku-string))))

;; TESTS
(def stackoverflow (read-sudoku "000070940070090005300005070087400100463000000000007080800700000700000028050268000"))
(def hardestsudokuinworld (read-sudoku "850002400720000009004000000000107002305000900040000000000080070017000000000036040"))
(def sudokuwiki-unsolvable-28 (read-sudoku "600008940900006100070040000200610000000000200089002000000060005000000030800001600"))
(def sudokuwiki-unsolvable-28-solution (read-sudoku "625178943948326157371945862257619384463587291189432576792863415516294738834751629"))

; point of highest abstraction
; Recursive depth-first search of solution
; returns solved board with first found solution
(defn solve [board]
  (let [[i cand-count] (p :find-least-candidates (find-least-candidates board))]
    (cond
      (= i 81) board ; solved
      (= cand-count 0) nil ; conflict, backtrack
      (= cand-count 1) (solve (set-value board i (first (nth board i))))
      :else (some #(solve (set-value board i %)) (nth board i))
      )))

;(let [s1 hardestsudokuinworld]
;  (println "Hardest Sudoku in the world:")
;  (println (pretty-print-board s1))
;  (println "solution:")
;  (println (time (pretty-print-board (solve s1)))))
;
;(let [sudoku sudokuwiki-unsolvable-28]
;  (println "Unsolvable #28, sudokuwiki.org:")
;  (println (pretty-print-board sudoku))
;  (println "solution:")
;  (let [reference sudokuwiki-unsolvable-28-solution
;        solution (time (solve sudoku))]
;    (println (pretty-print-board solution))
;    (println "Equals with reference solution: " (= reference solution))))

; (profile :info :Arithmetic (solve sudokuwiki-unsolvable-28))