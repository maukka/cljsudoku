(ns net.sirkia.core 
  (:require [taoensso.timbre :as timbre]
            [clojure.data.priority-map :refer [priority-map]]))
(timbre/refer-timbre)

(defn calculate-neighbor-indexes [i]
  "Return set of indexes of the neighbors excluding i itself. 0 <= i < 81"
  (let [r (int (/ i 9))
        c (mod i 9)
        gr (int (/ r 3))
        gc (int (/ c 3))
        row-indexes (map #(+ (* r 9) %) (range 9))
        col-indexes (map #(+ (* % 9) c) (range 9))
        block-indexes
        (for [ri (range (* (int gr) 3) (* (+ (int gr) 1) 3))
              ci (range (* (int gc) 3) (* (+ (int gc) 1) 3))]
          (+ (* ri 9) ci))]
    (disj (into #{} (flatten [row-indexes col-indexes block-indexes])) i)))

(def neighbors-table
  (vec (for [i (range 81)] (calculate-neighbor-indexes i))))

; Sudoku board is 1-dimensional vector 
; cell with value is just integer, 
; cell without value is set of possible values #{1 2 3 4 5 6 7 8 9}
;
; r = 0..8
; c = 0..8
; value = 1..9

; Board is [board priority-queue]
(defn new-board []
  [(vec (for [i (range 81)] (set (range 1 10))))
   (apply priority-map (flatten (for [i (range 81)] (list i 9))))])

(defn pretty-print-cell [cell]
  (format "%10s" (if (set? cell) (apply str cell) (str "val: " cell))))
(defn pretty-print-row [row]
  (apply pr-str (map pretty-print-cell row) "\n"))
(defn pretty-print-board [board]
  (map pretty-print-row (partition 9 board)))

; Return [new-board new-priority-queue]
(defn set-value [[board q] n new-value]
  "Return new board with cell n = new-value and new-value removed from all neighbors"
  (let [neighbor-indexes (nth neighbors-table n)
        tboard (transient board)
        q (reduce (fn [q i] 
                    (if (and (set? (nth board i)) (contains? (nth board i) new-value)) 
                      (let [newcell (disj (nth board i) new-value)
                            new-count (count newcell)]
                        (assoc! tboard i newcell)
                        (assoc q i new-count))
                      q))
                  q neighbor-indexes)
        ]
    (assoc! tboard n new-value) 
    [(persistent! tboard) (dissoc q n)]))

(defn read-sudoku [sudoku-string]
  (reduce (fn [[brd q] [i val]] (if (pos? val) (set-value [brd q] i val) [brd q])) 
          (new-board) 
          (map-indexed vector (map #(- (int %) 48) sudoku-string))))

;; TESTS
(def stackoverflow (read-sudoku "000070940070090005300005070087400100463000000000007080800700000700000028050268000"))
(def hardestsudokuinworld (read-sudoku "600008940900006100070040000200610000000000200089002000000060005000000030800001600"))
(def sudokuwiki-unsolvable-28 (read-sudoku "600008940900006100070040000200610000000000200089002000000060005000000030800001600"))
(def sudokuwiki-unsolvable-28-solution (read-sudoku "625178943948326157371945862257619384463587291189432576792863415516294738834751629"))

; point of highest abstraction
; Recursive depth-first search of solution
; returns solved board with first found solution
;
; This is fastest solution so far
(defn solve [[board q]]
 (if (empty? q)
   board ; solved
   (let [[i cand-count] (first q)
         candidates (nth board i)]
     (if (= cand-count 0)
       nil ; conflict, must backtrack
       (some #(solve (set-value [board q] i %)) candidates)
        ))))

; solves sudokuwiki-unsolvable-28 in 0,29 seconds (Java 8u25)
(time (solve sudokuwiki-unsolvable-28))

;(let [s1 hardestsudokuinworld]
;  (println "Hardest Sudoku in the world:")
;  (println (pretty-print-board s1))
;  (println "solution:")
;  (println (time (pretty-print-board (solve s1)))))
;
;(let [sudoku sudokuwiki-unsolvable-28]
;  (println "Unsolvable #28, sudokuwiki.org:")
;  (println (pretty-print-board sudoku))
;  (println "solution:")
;  (let [reference sudokuwiki-unsolvable-28-solution
;        solution (time (solve sudoku))]
;    (println (pretty-print-board solution))
;    (println "Equals with reference solution: " (= reference solution))))

;(profile :info :Arithmetic (solve sudokuwiki-unsolvable-28))